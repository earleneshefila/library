from django.shortcuts import render

# Create your views here.

def book_search(request):
    return render(request, 'library.html')
